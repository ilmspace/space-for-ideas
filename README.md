# Space for Ideas

The purpose of this repository is to manage TODOs and ideas using [issues](https://gitlab.com/ilmspace/space-for-ideas/-/boards).

There are two types of issues:
- `todo`: TODOs to do regarding the Space
- `idea`: Idea for a cool project someone (maybe yourself ;)) could work on